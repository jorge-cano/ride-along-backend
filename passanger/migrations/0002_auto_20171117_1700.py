# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-11-17 17:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('passanger', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rutepoints',
            name='driver',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='passanger.Driver'),
        ),
    ]
