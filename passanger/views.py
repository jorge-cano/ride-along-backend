from decimal import Decimal

import json

from datetime import datetime

from django.contrib.gis.geos.geometry import GEOSGeometry
from django.http.response import HttpResponse

from django.contrib.gis.geos.point import Point
from django.shortcuts import render

# Create your views here.
from django.views.generic.base import View

from passanger.functions import get_matching_rutes
from passanger.models import Driver, RutePoints
from passanger.serializers import ExtJsonSerializer


class RuteFinder(View):

    def get(self, request, s_latitude, s_longitude):

        point_description ='POINT ({0} {1})'.format(s_longitude, s_latitude)
        user_location = GEOSGeometry(point_description, srid=4326)

        matching_rutes = get_matching_rutes(user_location)

        serialized_matches = ExtJsonSerializer().serialize(
            queryset=matching_rutes,
            props=[]
        )

        return HttpResponse(serialized_matches, content_type="application/json")


class DriverRuteFinder(View):

    def get(self, request, s_driver_id):

        if Driver.objects.filter(id=int(s_driver_id)).exists():

            driver = Driver.objects.get(id=int(s_driver_id))
            serialized_rute = ExtJsonSerializer().serialize(
                queryset=RutePoints.objects.filter(driver=driver),
                props=[]
            )
            return HttpResponse(serialized_rute, content_type="application/json")

        else:
            return HttpResponse('{"message": "Driver not found"}', content_type="application/json", status=404)


class DriverRegister(View):

    def post(self, request):
        # sensor_data = json.loads(request.POST["data"]) #para x-www-form-urlencoded
        driver_info = json.loads(request.body.decode('utf-8'))  # para body (byte string)

        if not Driver.objects.filter(student_id=driver_info['student_id']).exists():

            entry_hour = datetime.strptime(driver_info['entry_hour'], '%H:%M').time()
            exit_hour = datetime.strptime(driver_info['exit_hour'], '%H:%M').time()

            new_driver = Driver(
                student_id=driver_info['student_id'],
                name=driver_info['name'],
                car_brand=driver_info['car_brand'],
                car_color=driver_info['car_color'],
                license_plate=driver_info['license_plate'],
                phonenumber=driver_info['phonenumber'],
                capacity=driver_info['capacity'],
                entry_hour=entry_hour,
                exit_hour=exit_hour
            )
            new_driver.save()

            for point in driver_info['rute_points']:

                RutePoints(
                    driver=new_driver,
                    latitude=Decimal(point['latitude']),
                    longitude=Decimal(point['longitude'])
                ).save()

            return HttpResponse('{"message": "All good"}', content_type="application/json")

        else:

            return HttpResponse('{"message": "Driver already registered"}', content_type="application/json", status=409)


class DriverInfoFinder(View):

    def get(self, request, student_id):

        if Driver.objects.filter(student_id=student_id).exists():

            driver = Driver.objects.filter(student_id=student_id)
            serialized_driver = ExtJsonSerializer().serialize(queryset=driver, props=[])[1: -1]

            rute = RutePoints.objects.filter(driver=driver)
            serialized_rute = ExtJsonSerializer().serialize(queryset=rute, props=[])

            driver_info = {
                "driver": serialized_driver,
                "rute": serialized_rute
            }

            return HttpResponse(json.dumps(driver_info), content_type="application/json")

        else:
            return HttpResponse('{"message": "Driver not found"}', content_type="application/json", status=404)
