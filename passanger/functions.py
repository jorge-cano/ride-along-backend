from passanger.models import Driver, RutePoints


def get_matching_rutes(user_point):

    matching_drivers = []
    for driver in Driver.objects.all():

        rute_points = RutePoints.objects.filter(driver=driver)
        match = is_rute_close(user_point, rute_points)

        if match:
            matching_drivers.append(driver)

    return matching_drivers


def is_rute_close(user_point, rute_points):

    reasonable_distance = 250
    user_point.transform(3857)

    for rute_point in rute_points:

        rute_coordinate = rute_point.get_as_point()
        rute_coordinate.transform(3857)

        distance = user_point.distance(rute_coordinate)

        if distance <= reasonable_distance:
            return True

    return False
