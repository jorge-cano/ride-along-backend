from django.contrib.gis.geos.geometry import GEOSGeometry
from django.contrib.gis.geos.point import Point
from django.db import models

# Create your models here.


class Driver(models.Model):
    student_id = models.CharField(max_length=9, unique=True)
    name = models.CharField(max_length=50)
    car_brand = models.CharField(max_length=50)
    car_color = models.CharField(max_length=50)
    license_plate = models.CharField(max_length=10)
    phonenumber = models.CharField(max_length=15)
    capacity = models.IntegerField()
    entry_hour = models.TimeField(auto_now=False, auto_now_add=False)
    exit_hour = models.TimeField(auto_now=False, auto_now_add=False)

    @property
    def rute(self):
        return RutePoints.objects.filter(driver=self)


class RutePoints(models.Model):
    driver = models.ForeignKey(Driver)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)

    def get_as_point(self):

        point_description ='POINT ({0} {1})'.format(
            str(self.longitude),
            str(self.latitude)
        )
        return GEOSGeometry(point_description, srid=4326)
